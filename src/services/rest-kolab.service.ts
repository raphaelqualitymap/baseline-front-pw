export async function postFormKolab(endpoint: string, payload: any, headers?: { [key: string]: string; }) {
  let body: any;
  let status: number | any;

  if (!globalThis.requestKolab) {
    throw new Error('requestKolab não foi inicializado corretamente.');
  }

  try {

    const request =  await requestKolab.post(endpoint, {
      form: payload,
      headers: headers
    });

    status =  request.status();
    body = await request.json();
   
  } catch(e) {
    return {
      'body': e,
      'status': status 
    };
  }
  
  return {
    'body': body,
    'status': status
  };

}

export async function postKolab(endpoint: string, payload: any, headers?: { [key: string]: string; }) {
  let body: any;
  let status: number | any;

  if (!globalThis.requestKolab) {
    throw new Error('requestKolab não foi inicializado corretamente.');
  }

  try {

    const request =  await requestKolab.post(endpoint, {
      data: payload,
      headers: headers
    });
    
    status = request.status();
    body = await request.json();
   
  } catch(e) {
    return {
      'body': e,
      'status': status
    };
  }
  
  return {
    'body': body,
    'status': status
  };
 

}

export async function putFormKolab(endpoint: string, payload: any, headers?: { [key: string]: string; }) {
  let body: any;
  let status: number | any;

  if (!globalThis.requestKolab) {
    throw new Error('requestKolab não foi inicializado corretamente.');
  }

  try {

    const request =  await requestKolab.put(endpoint, {
      form: payload,
      headers: headers
    });

    status = await request.status();
    body = request.json();

   
  } catch(e) {
    return {
      'body': e,
      'status': status
    };
  }
  
  return {
    'body': body,
    'status': status
  };

}

export async function getKolab(endpoint: string, headers?: { [key: string]: string; }) {
  let body: any;
  let status: number | any;
  if (!globalThis.requestKolab) {
    throw new Error('interactMsAgentes não foi inicializado corretamente.');
  }
  try {

    const request =  await requestKolab.get(endpoint, {
      headers: headers
    });
  
    status = request.status();
    body = await request.json();
   
  } catch(e) {
    return {
      'body': e,
      'status': status
    };
  }
  
  return {
    'body': body,
    'status': status
  };
}

export async function getQueryKolab(endpoint: string, queryParam: string, headers?: { [key: string]: string; }) {
  let body: any;
  let status: number | any;
  if (!globalThis.requestKolab) {
    throw new Error('interact Kolab não foi inicializado corretamente.');
  }
  try {

    const request =  await requestKolab.get(`${endpoint}?${queryParam}`, {
      headers: headers
    });
  
    status = request.status();
    body = await request.json();
   
  } catch(e) {
    return {
      'body': e,
      'status': status
    };
  }
  
  return {
    'body': body,
    'status': status
  };
}